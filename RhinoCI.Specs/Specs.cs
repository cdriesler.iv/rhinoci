﻿using RhinoCI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using Rhino.Geometry;

namespace RhinoCI.Specs
{
    [TestClass]
    public class Specs
    {
        [TestMethod]
        public void Control()
        {
            true.Should().BeTrue("because otherwise we have a problem");
        }

        [TestMethod]
        public void Given_disjoint_curves_should_return_false()
        {
            var curveA = new LineCurve(new Point3d(-1, 0, 0), new Point3d(-1, 1, 0));
            var curveB = new LineCurve(new Point3d(1, 0, 0), new Point3d(1, 1, 0));

            var result = Verify.IsIntersection(curveA, curveB);

            result.Should().BeFalse("because the curves do not intersect");
        }

        [TestMethod]
        public void Given_intersecting_curves_should_return_true()
        {
            var curveA = new LineCurve(new Point3d(-1, 0, 0), new Point3d(1, 0, 0));
            var curveB = new LineCurve(new Point3d(0, -1, 0), new Point3d(0, 1, 0));

            var result = Verify.IsIntersection(curveA, curveB);

            result.Should().BeTrue("because the curves intersect");
        }
    }
}
