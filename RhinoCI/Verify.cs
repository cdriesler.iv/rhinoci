﻿using Rhino.Geometry;
using Rhino.Geometry.Intersect;
using System.Linq;

namespace RhinoCI
{
    public static class Verify
    {
        public static bool IsIntersection(Curve curveA, Curve curveB)
        {
            return Intersection.CurveCurve(curveA, curveB, 0.1, 0.1).Any();
        }
    }
}
